import javax.swing.*;
import javax.swing.table.TableModel;
import java.awt.event.*;
import java.util.List;
import java.util.ArrayList;

/**
 * Реализация окна редактирования групп.
 */
public class EditGroupsWindow extends JFrame {
    private JFrame frame;
    private JPanel panelEditGroups;
    private JList listGroups;
    private JButton buttonCreateGroup;
    private JButton buttonDelGroup;
    private JButton buttonAddInGroup;
    private JButton buttonDelFromGroup;
    private JButton buttonRenameGroup;
    private JList listUsers;
    private JList listUsersInGroup;

    private ArrayList<MoodleUser> users;
    private DefaultListModel groupsModel = new DefaultListModel();
    private DefaultListModel usersModel = new DefaultListModel();
    private DefaultListModel usersInGroupModel = new DefaultListModel();

    /**
     * Добавляет группу {@param name} в список групп.
     */
    public void addGroup(String name) {
        Config.addGroup(name);
        groupsModel.addElement(name);
    }

    /**
     * Добавляет пользователя в группу.
     * @param groupId - порядковый номер группы в массиве groups.
     * @param userIdInList - порядковый номер пользователя в массиве users.
     * @param userId - id пользователя, добавляемого в группу.
     * @param firstname - имя пользователя.
     * @param lastname - фамилия
     */
    public void addUserInGroup(int groupId, int userIdInList, int userId, String firstname, String lastname) {
        usersInGroupModel.addElement(firstname + " " + lastname);
        Config.addUserInGroup(groupId, userId, firstname, lastname);
        users.remove(userIdInList);
        usersModel.remove(userIdInList);
    }

    /**
     * Удаляет пользователя из группы.
     * @param groupId - порядковый номер группы в массиве groups.
     * @param userId - id пользователя, удаляемого из группы.
     */
    public void delUserFromGroup(int groupId, int userId) {
        MoodleUser user = Config.getUsersInGroup(groupId).get(userId);
        usersModel.addElement(user.firstname + " " + user.lastname);
        usersInGroupModel.remove(userId);
        Config.delUserFromGroup(groupId, userId);
        users.add(user);
    }

    public EditGroupsWindow(final List<Group> groups) {
        frame = this;
        this.getContentPane().add(panelEditGroups);

        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

        // Приёмник события закрытия окна.
        this.addWindowListener(new WindowListener() {

            public void windowActivated(WindowEvent event) {}
            public void windowClosed(WindowEvent event) {}
            public void windowDeactivated(WindowEvent event) {}
            public void windowDeiconified(WindowEvent event) {}
            public void windowIconified(WindowEvent event) {}
            public void windowOpened(WindowEvent event) {}

            public void windowClosing(WindowEvent event) {
                MoodleLogstoreViewer.reloadGroups();
                frame.setVisible(false);
                frame.dispose();
            }

        });

        listGroups.setModel(groupsModel);
        for (Group item : groups) {
            groupsModel.addElement(item.name);
        }

        listGroups.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int groupID = listGroups.getAnchorSelectionIndex();

                // отображение списка пользователей, которых можно добавить в группу
                users = Database.getUsers(Config.getUsersInGroup(groupID));
                listUsers.setModel(usersModel);
                usersModel.removeAllElements();
                for (MoodleUser item : users) {
                    usersModel.addElement(item.firstname + " " + item.lastname);
                }

                // отображение списка пользователей, которые уже добавлены в группу
                ArrayList<MoodleUser> usersInGroup = groups.get(groupID).users;
                if (usersInGroup == null) {
                    usersInGroup = new ArrayList();
                }
                listUsersInGroup.setModel(usersInGroupModel);
                usersInGroupModel.removeAllElements();
                for (MoodleUser item : usersInGroup) {
                    usersInGroupModel.addElement(item.firstname + " " + item.lastname);
                }
            }
        });

        buttonCreateGroup.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String result = JOptionPane.showInputDialog(frame, "Введите название будущей группы:",
                        "Группа ##");
                addGroup(result);
            }
        });



        /**
         * Приёмник на кнопку buttonRenameGroup.
         */
        buttonRenameGroup.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // Если ничего не выбрано из групп, значит и переименовывать нечего.
                if (listGroups.isSelectionEmpty() == false) {
                    // Получаем текущий порядковый номер пункта в JList listGroups,
                    // по совместительству он совпадает с порядковым номером данной
                    // группы в массиве groups. А также его название.
                    int groupID = listGroups.getAnchorSelectionIndex();
                    String oldName = groups.get(groupID).name;

                    // Ожидаем, пока пользователь введёт новое название.
                    String newName = JOptionPane.showInputDialog(frame,
                            "Введите новое название группы:",
                            oldName);

                    // Подстраховка на случай, если пользователь нажал отмена или
                    // оставил поле пустым.
                    if (newName != null && !newName.equals("")) {
                        // Переименовываем текущий пункт в JList listGroups и изменяем
                        // название внутри структуры Config.
                        groupsModel.remove(groupID);
                        groupsModel.add(groupID, newName);
                        Config.renameGroup(groupID, newName);
                    }
                }
            }
        });


        /**
         * Приёмник на кнопку buttonDelGroup.
         */
        buttonDelGroup.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // Если ничего не выбрано из групп, значит и удалять нечего.
                if (listGroups.isSelectionEmpty() == false) {
                    // Получаем текущий порядковый номер пункта в JList listGroups,
                    // по совместительству он совпадает с порядковым номером данной
                    // группы в массиве groups.
                    int groupID = listGroups.getAnchorSelectionIndex();

                    // Удаляем текущий пункт из JList listGroups и структуры Config.
                    groupsModel.remove(groupID);
                    Config.delGroup(groupID);
                }
            }
        });

        /**
         * Приёмник на кнопку buttonAddInGroup.
         */
        buttonAddInGroup.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // Если никто из пользователей не выбран, значит и добавлять в группу нечего.
                if (listUsers.isSelectionEmpty() == false) {
                    // Получаем текущий порядковый номер пункта в JList listGroups,
                    // по совместительству он совпадает с порядковым номером данной
                    // группы в массиве groups.
                    int groupID = listGroups.getAnchorSelectionIndex();

                    // Получаем текущий порядковый номер пункта в JList listUsers,
                    // по совместительству он совпадает с порядковым номером данной
                    // группы в массиве users.
                    int userIdInList = listUsers.getAnchorSelectionIndex();

                    // Добавляем этого пользователя в группу.
                    addUserInGroup(groupID, userIdInList,
                            users.get(userIdInList).id,
                            users.get(userIdInList).firstname,
                            users.get(userIdInList).lastname);
                }
            }
        });

        /**
         * Приёмник на кнопку buttonDelFromGroup.
         */
        buttonDelFromGroup.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // Если ничего не выбрано из групп, значит и удалять нечего.
                if (listGroups.isSelectionEmpty() == false) {
                    // Получаем текущий порядковый номер пункта в JList listGroups,
                    // по совместительству он совпадает с порядковым номером данной
                    // группы в массиве groups.
                    int groupId = listGroups.getAnchorSelectionIndex();

                    // Получаем текущий порядковый номер пункта в JList listUsersInGroup.
                    int userId = listUsersInGroup.getAnchorSelectionIndex();

                    // Удаляем этого пользователя из группы.
                    delUserFromGroup(groupId, userId);
                }
            }
        });

    }

}


import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;

import java.io.*;
import java.util.ArrayList;

public class Export {

    /**
     * Создаёт файл {@param fileName}, где в формате CSV будет записаны данные из {@param logstore}.
     * В качестве разделителя используется точка с запятой.
     */
    public static void toCSV(ArrayList<MoodleLog> logstore, String fileName) {
        File file = new File(fileName);

        try {
            if (!file.exists()) {
                file.createNewFile();
                PrintWriter out = new PrintWriter(file.getAbsoluteFile());

                StringBuilder stringBuild = new StringBuilder();

                // Создаём строку с названиями столбцов.
                stringBuild.append("id;");
                stringBuild.append("username;");
                stringBuild.append("eventname;");
                stringBuild.append("component;");
                stringBuild.append("action;");
                stringBuild.append("target;");
                stringBuild.append("date;");
                stringBuild.append("time;");
                stringBuild.append("origin;");
                stringBuild.append("ip");
                stringBuild.append("\r\n");

                // Заполняем stringBuild содержимым logstore.
                for (MoodleLog item : logstore) {
                    stringBuild.append(item.id);
                    stringBuild.append(";");
                    stringBuild.append(item.username);
                    stringBuild.append(";");
                    stringBuild.append(item.eventname);
                    stringBuild.append(";");
                    stringBuild.append(item.component);
                    stringBuild.append(";");
                    stringBuild.append(item.action);
                    stringBuild.append(";");
                    stringBuild.append(item.target);
                    stringBuild.append(";");
                    stringBuild.append(MoodleDateFormat.getDateIgnoreTime(item.timecreated));
                    stringBuild.append(";"); // Не забываем разделитель между столбцами.
                    stringBuild.append(MoodleDateFormat.getOnlyTime(item.timecreated));
                    stringBuild.append(";");
                    stringBuild.append(item.origin);
                    stringBuild.append(";");
                    stringBuild.append(item.ip);
                    stringBuild.append("\r\n");
                }

                try {
                    out.print(stringBuild.toString());
                } finally {
                    out.close();
                }
            } else {
                System.out.println("Файл " + fileName + " уже существует. Укажите другой путь.");
            }
        } catch(IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Создаёт Excel документ {@param fileName}, где будут записаны данные из {@param logstore}.
     * @throws FileNotFoundException
     * @throws IOException
     */
    public static void toXLSX(ArrayList<MoodleLog> logstore, String file) throws FileNotFoundException, IOException{
        // todo проблема: создаёт не xlsx, а xls.
        Workbook book = new HSSFWorkbook();
        Sheet sheet = book.createSheet( );
        Row row;
        Cell cell;
        int row_num = 0;
        int cell_num = 0;

        // Создаём строку с названиями столбцов.
        row = sheet.createRow(row_num++);

        cell = row.createCell(cell_num++);
        sheet.autoSizeColumn(cell_num);
        cell.setCellValue("id");

        cell = row.createCell(cell_num++);
        sheet.autoSizeColumn(cell_num);
        cell.setCellValue("username");

        cell = row.createCell(cell_num++);
        sheet.autoSizeColumn(cell_num);
        cell.setCellValue("eventname");

        cell = row.createCell(cell_num++);
        sheet.autoSizeColumn(cell_num);
        cell.setCellValue("component");

        cell = row.createCell(cell_num++);
        sheet.autoSizeColumn(cell_num);
        cell.setCellValue("action");

        cell = row.createCell(cell_num++);
        sheet.autoSizeColumn(cell_num);
        cell.setCellValue("target");

        cell = row.createCell(cell_num++);
        sheet.autoSizeColumn(cell_num);
        cell.setCellValue("date");

        cell = row.createCell(cell_num++);
        sheet.autoSizeColumn(cell_num);
        cell.setCellValue("time");

        cell = row.createCell(cell_num++);
        sheet.autoSizeColumn(cell_num);
        cell.setCellValue("origin");

        cell = row.createCell(cell_num++);
        sheet.autoSizeColumn(cell_num);
        cell.setCellValue("ip");

        // Создаём последующие строки по данным массива logstore.
        for (MoodleLog item : logstore) {
            cell_num = 0;
            row = sheet.createRow(row_num++);

            cell = row.createCell(cell_num++);
            sheet.autoSizeColumn(cell_num);
            cell.setCellValue(item.id);

            cell = row.createCell(cell_num++);
            sheet.autoSizeColumn(cell_num);
            cell.setCellValue(item.username);

            cell = row.createCell(cell_num++);
            sheet.autoSizeColumn(cell_num);
            cell.setCellValue(item.eventname);

            cell = row.createCell(cell_num++);
            sheet.autoSizeColumn(cell_num);
            cell.setCellValue(item.component);

            cell = row.createCell(cell_num++);
            sheet.autoSizeColumn(cell_num);
            cell.setCellValue(item.action);

            cell = row.createCell(cell_num++);
            sheet.autoSizeColumn(cell_num);
            cell.setCellValue(item.target);

            cell = row.createCell(cell_num++);
            sheet.autoSizeColumn(cell_num);
            cell.setCellValue(MoodleDateFormat.getDateIgnoreTime(item.timecreated));

            cell = row.createCell(cell_num++);
            sheet.autoSizeColumn(cell_num);
            cell.setCellValue(MoodleDateFormat.getOnlyTime(item.timecreated));

            cell = row.createCell(cell_num++);
            sheet.autoSizeColumn(cell_num);
            cell.setCellValue(item.origin);

            cell = row.createCell(cell_num++);
            sheet.autoSizeColumn(cell_num);
            cell.setCellValue(item.ip);
        }

        book.write(new FileOutputStream(file));
        book.close();
    }

}

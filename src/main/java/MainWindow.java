import javax.swing.*;
import javax.swing.table.TableModel;
import java.awt.event.*;
import java.io.IOException;
import java.util.*;

/**
 * Реализация главного окна.
 */
public class MainWindow extends JFrame {
    private JPanel panel;
    private JTable tableViewer;
    private JTable tableCourses;
    private JTable tableUsers;
    private JButton buttonExportToCSV;
    private JButton buttonExportToXLSX;
    private JTabbedPane tabbedUsers;
    private JList listGroups;
    private JButton buttonEditGroupsList;
    private JList listDate;
    private JList listActions;
    private JFileChooser fileChooser;

    private ArrayList<MoodleLog> logstore = null;
    private ArrayList<MoodleLog> filteredLogstore = null;

    private List<MoodleCourse> courses;
    private List<MoodleUser> users;
    private List<Group> groups;

    private int currentUserID = 0;
    private int currentCourseID = 0;
    private String currentDate = null;
    private String currentAction = null;

    public MainWindow() {

        // Создание экземпляра JFileChooser
        fileChooser = new JFileChooser();

        this.getContentPane().add(panel);

        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

        /**
         * Приёмник события закрытия окна.
         */
        this.addWindowListener(new WindowListener() {

            public void windowActivated(WindowEvent event) {}
            public void windowClosed(WindowEvent event) {}
            public void windowDeactivated(WindowEvent event) {}
            public void windowDeiconified(WindowEvent event) {}
            public void windowIconified(WindowEvent event) {}
            public void windowOpened(WindowEvent event) {}

            public void windowClosing(WindowEvent event) {
                MoodleLogstoreViewer.exit();
            }

        });

        listGroups.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                pasteLogstore(listGroups.getAnchorSelectionIndex());
            }
        });

        listActions.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (listActions.isSelectionEmpty() == false && listActions.getSelectedIndex() != 0) {
                    currentAction = (String) listActions.getSelectedValue();
                } else {
                    currentAction = null;
                }
                pasteFilteredLogstore();
            }
        });

        listDate.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (listDate.isSelectionEmpty() == false && listDate.getSelectedIndex() != 0) {
                    currentDate = (String) listDate.getSelectedValue();
                } else {
                    currentDate = null;
                }
                pasteFilteredLogstore();
                pasteActions();
            }
        });

        tableCourses.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                currentCourseID = courses.get(tableCourses.getSelectedRow()).id;
                pasteLogstore();
            }
        });

        tableUsers.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                currentUserID = users.get(tableUsers.getSelectedRow()).id;
                pasteLogstore();
            }
        });

        buttonEditGroupsList.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                MoodleLogstoreViewer.openEditGroupsWindow();
            }
        });

        buttonExportToCSV.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                fileChooser.setDialogTitle("Сохранение файла");
                // Определение режима - только файл
                fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
                int result = fileChooser.showSaveDialog(panel);
                // Если файл выбран, то представим его в сообщении
                if (result == JFileChooser.APPROVE_OPTION ) {
                    Export.toCSV(logstore, fileChooser.getSelectedFile().toString());
                    JOptionPane.showMessageDialog(panel, "Файл '" + fileChooser.getSelectedFile() + "' сохранен");
                }
            }
        });

        buttonExportToXLSX.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                fileChooser.setDialogTitle("Сохранение файла");
                // Определение режима - только файл
                fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
                int result = fileChooser.showSaveDialog(panel);
                // Если файл выбран, то представим его в сообщении
                if (result == JFileChooser.APPROVE_OPTION ) {
                    try {
                        Export.toXLSX(logstore, fileChooser.getSelectedFile().toString());
                        JOptionPane.showMessageDialog(panel, "Файл '" + fileChooser.getSelectedFile() + "' сохранен");
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
            }
        });

    }

    /**
     * Пытается отобразить логи для текущей комбинации курса - пользователя (currentUserID и currentCourseID).
     * Срабатывает только при условии наличия данных для переменных currentUserID и currentCourseID.
     */
    public void pasteLogstore() {
        if (currentUserID != 0 && currentCourseID != 0) {
            ArrayList<MoodleLog> logstore = Database.getLogstore(currentUserID, currentCourseID);
            this.logstore = logstore;
            filteredLogstore = null;

            TableModel logstoreModel = new LogstoreModel(logstore);
            tableViewer.setModel(logstoreModel);

            pasteDates();
            pasteActions();
        }
    }

    /**
     * Пытается отобразить логи для текущей комбинации курса - группы.
     * Срабатывает только при условии наличия данных в переменной currentCourseID.
     * @param groupID
     */
    public void pasteLogstore(int groupID) {
        if (currentCourseID != 0) {
            Group item = groups.get(groupID);
            ArrayList<MoodleLog> logstore = Database.getLogstore(item.users, currentCourseID);
            this.logstore = logstore;
            TableModel logstoreModel = new LogstoreModel(logstore);
            tableViewer.setModel(logstoreModel);

            pasteDates();
            pasteActions();
        }
    }

    /**
     * Фильтрует текущий лог, подробности см. в Utils.getFilteredLogstore().
     */
    public void pasteFilteredLogstore() {
        filteredLogstore = Utils.getFilteredLogstore(logstore, currentDate, currentAction);
        TableModel logstoreModel = new LogstoreModel(filteredLogstore);
        tableViewer.setModel(logstoreModel);
    }

    /**
     * Отображает список дат для текущего лога.
     */
    public void pasteDates() {
        ArrayList<Date> dates = Utils.getDateList(logstore);

        DefaultListModel dateModel = new DefaultListModel();
        listDate.setModel(dateModel);

        dateModel.addElement("Все записи");
        listDate.addSelectionInterval(0, 0);
        currentDate = null;
        for (Date date : dates) {
            dateModel.addElement(MoodleDateFormat.getDateIgnoreTime(date));
        }
    }

    /**
     * Отображает список действий для текущего лога.
     */
    public void pasteActions() {
        ArrayList<String> actions;

        if (filteredLogstore != null) {
            actions = Utils.getActions(filteredLogstore);
        } else {
            actions = Utils.getActions(logstore);
        }


        DefaultListModel actionsModel = new DefaultListModel();
        listActions.setModel(actionsModel);

        actionsModel.addElement("Все действия");
        listActions.addSelectionInterval(0, 0);
        currentAction = null;

        for (String action : actions) {
            actionsModel.addElement(action);
        }
    }

    /**
     * Отображает список курсов {@param courses} в JTable tableCourses.
     */
    public void pasteCourses(List<MoodleCourse> courses) {
        this.courses = courses;
        TableModel coursesModel = new CoursesModel(courses);
        tableCourses.setModel(coursesModel);
    }

    /**
     * Отображает список пользователей {@param users} в JTable tableUsers.
     */
    public void pasteUsers(List<MoodleUser> users) {
        this.users = users;
        TableModel coursesModel = new UsersModel(users);
        tableUsers.setModel(coursesModel);
    }

    /**
     * Отображает список групп {@param groups} в JList listGroups.
     */
    public void pasteGroups(List<Group> groups) {
        this.groups = groups;
        DefaultListModel listModel = new DefaultListModel();
        listGroups.setModel(listModel);

        for (Group item : groups) {
            listModel.addElement(item.name);
        }
    }

}

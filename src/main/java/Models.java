import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Описание модели списка курсов для JTable tableCourses.
 */
class CoursesModel implements TableModel {

    private Set<TableModelListener> listeners = new HashSet<TableModelListener>();

    private List<MoodleCourse> courses;

    public CoursesModel(List<MoodleCourse> courses) {
        this.courses = courses;
    }

    public void addTableModelListener(TableModelListener listener) {
        listeners.add(listener);
    }

    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return int.class;
            default:
                return String.class;
        }
    }

    public int getColumnCount() {
        return 2;
    }

    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return "ID";
            case 1:
                return "Курс";
        }
        return "";
    }

    public int getRowCount() {
        return courses.size();
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        MoodleCourse item = courses.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return item.id;
            case 1:
                return item.fullname;
        }
        return "";
    }

    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    public void removeTableModelListener(TableModelListener listener) {
        listeners.remove(listener);
    }

    public void setValueAt(Object value, int rowIndex, int columnIndex) {

    }

}

/**
 * Описание модели списка пользователей для JTable tableUsers.
 */
class UsersModel implements TableModel {

    private Set<TableModelListener> listeners = new HashSet<TableModelListener>();

    private List<MoodleUser> users;

    public UsersModel(List<MoodleUser> users) {
        this.users = users;
    }

    public void addTableModelListener(TableModelListener listener) {
        listeners.add(listener);
    }

    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return int.class;
            default:
                return String.class;
        }
    }

    public int getColumnCount() {
        return 2;
    }

    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return "ID";
            case 1:
                return "Пользователь";
        }
        return "";
    }

    public int getRowCount() {
        return users.size();
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        MoodleUser item = users.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return item.id;
            case 1:
                return item.firstname + " " + item.lastname;
        }
        return "";
    }

    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    public void removeTableModelListener(TableModelListener listener) {
        listeners.remove(listener);
    }

    public void setValueAt(Object value, int rowIndex, int columnIndex) {

    }

}

/**
 * Описание модели лога для JTable tableViewer.
 */
class LogstoreModel implements TableModel {

    private Set<TableModelListener> listeners = new HashSet<TableModelListener>();

    private List<MoodleLog> logstore;

    public LogstoreModel(List<MoodleLog> logstore) {
        this.logstore = logstore;
    }

    public void addTableModelListener(TableModelListener listener) {
        listeners.add(listener);
    }

    public Class<?> getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return int.class;
            case 7:
                return long.class;
            default:
                return String.class;
        }
    }

    public int getColumnCount() {
        return 8;
    }

    public String getColumnName(int columnIndex) {
        switch (columnIndex) {
            case 0:
                return "ID";
            case 1:
                return "Пользователь";
            case 2:
                return "Описание";
            case 3:
                return "Компонент";
            case 4:
                return "Действие";
            case 5:
                return "Цель";
            case 6:
                return "Время";
            case 7:
                return "Origin";
            case 8:
                return "IP";
        }
        return "";
    }

    public int getRowCount() {
        return logstore.size();
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        MoodleLog logItem = logstore.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return logItem.id;
            case 1:
                return logItem.username;
            case 2:
                return logItem.eventname;
            case 3:
                return logItem.component;
            case 4:
                return logItem.action;
            case 5:
                return logItem.target;
            case 6:
                return MoodleDateFormat.getFormat(logItem.timecreated);
            case 7:
                return logItem.origin;
            case 8:
                return logItem.ip;
        }
        return "";
    }

    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    public void removeTableModelListener(TableModelListener listener) {
        listeners.remove(listener);
    }

    public void setValueAt(Object value, int rowIndex, int columnIndex) {

    }

}

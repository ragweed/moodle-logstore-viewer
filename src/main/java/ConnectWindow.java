import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.sql.SQLException;

/**
 * Реализация окна соединения с БД.
 */
public class ConnectWindow extends JFrame {
    private JFrame frame;
    private JTextField fieldHost;
    private JTextField fieldPort;
    private JTextField fieldDB;
    private JTextField fieldUser;
    private JPasswordField fieldPassword;
    private JButton buttonAuth;
    private JButton buttonCancel;
    private JPanel panelAuth;

    public ConnectWindow() {
        frame = this;
        this.getContentPane().add(panelAuth);

        frame.addWindowListener(new WindowListener() {

            public void windowActivated(WindowEvent event) {

            }

            public void windowClosed(WindowEvent event) {

            }

            public void windowClosing(WindowEvent event) {
                System.exit(0);
            }

            public void windowDeactivated(WindowEvent event) {

            }

            public void windowDeiconified(WindowEvent event) {

            }

            public void windowIconified(WindowEvent event) {

            }

            public void windowOpened(WindowEvent event) {

            }

        });

        buttonAuth.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Database.open(fieldHost.getText(),
                            Integer.parseInt(fieldPort.getText()),
                            fieldDB.getText(),
                            fieldUser.getText(),
                            String.valueOf(fieldPassword.getPassword()));
                    panelAuth.setVisible(false);
                    frame.dispose();
                    MoodleLogstoreViewer.openMainWindow();
                } catch (SQLException se) {
                    JOptionPane.showMessageDialog(null, "Не удалось соединится с базой данных.\nПричина: "
                            + se.getMessage(), "Ошибка", JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

    }

}


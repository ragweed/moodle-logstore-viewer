import java.sql.*;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * Предоставляет статичные методы для взаимодействия с БД moodle.
 */
public class Database {

    private static Connection con = null;

    /**
     * Закрывает соединение с БД.
     */
    public static void close() {
        try {
            con.close();
        } catch(SQLException se) {
            se.printStackTrace();
        }
    }

    /**
     * Соединяет с БД и позволяет использовать методы для взаимодействия с БД.
     * @param host
     * @param port
     * @param dbName
     * @param username
     * @param password
     * @throws SQLException
     */
    public static void open(String host, int port, String dbName, String username, String password) throws SQLException {
        String url = "jdbc:mysql://" + host + ":" + port + "/" + dbName +
                "?useUnicode=true&characterEncoding=utf8&useSSL=false";
        con = DriverManager.getConnection(url, username, password);
    }

    /**
     * Возращает логи из БД в виде массива объектов MoodleLog для пользователя userId на курсе courseId.
     * @param userId
     * @param courseId
     * @return массив объектов MoodleLog
     */
    public static ArrayList<MoodleLog> getLogstore(int userId, int courseId) {
        ArrayList logstore = new ArrayList<MoodleLog>();
        String query = "SELECT id, " +
                "(SELECT CONCAT_WS(' ', firstname, lastname) FROM mdl_user WHERE id = log.userid) as username," +
                "eventname, component, action, target, timecreated, origin, ip " +
                "FROM mdl_logstore_standard_log AS log WHERE userid=? AND courseid=? ORDER BY timecreated DESC";

        PreparedStatement prStmt = null;
        ResultSet rs = null;

        try {
            prStmt = con.prepareStatement(query);
            prStmt.setInt(1, userId);
            prStmt.setInt(2, courseId);
            rs = prStmt.executeQuery();

            while (rs.next()) {
                MoodleLog item = new MoodleLog();
                item.id = rs.getInt("id");
                item.username = rs.getString("username");
                item.eventname = rs.getString("eventname");
                item.component = rs.getString("component");
                item.action = rs.getString("action");
                item.target = rs.getString("target");
                item.timecreated = new Date(rs.getLong("timecreated") * 1000);
                item.origin = rs.getString("origin");
                item.ip = rs.getString("ip");
                logstore.add(item);
            }
        } catch (SQLException err) {
            err.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (prStmt != null) prStmt.close();
            } catch (SQLException err) {
                err.printStackTrace();
            }
        }

        return logstore;
    }

    /**
     * Возращает логи из БД в виде массива объектов MoodleLog для пользователей из списка users.
     * @param users
     * @param courseId
     * @return массив объектов MoodleLog
     */
    public static ArrayList<MoodleLog> getLogstore(ArrayList<MoodleUser> users, int courseId) {
        ArrayList logstore = new ArrayList<MoodleLog>();

        StringBuffer usersID = new StringBuffer();
        Iterator it = users.iterator();
        while (it.hasNext()) {
            MoodleUser user = (MoodleUser) it.next();
            usersID.append(Integer.toString(user.id));
            if (it.hasNext()) {
                usersID.append(",");
            }
        }

        String query = "SELECT id, " +
                "(SELECT CONCAT_WS(' ', firstname, lastname) FROM mdl_user WHERE id = log.userid) as username," +
                "eventname, component, action, target, timecreated, origin, ip " +
                "FROM mdl_logstore_standard_log AS log WHERE userid IN(" +
                usersID.toString() + ") AND courseid=? ORDER BY timecreated DESC";

        PreparedStatement prStmt = null;
        ResultSet rs = null;

        try {

            prStmt = con.prepareStatement(query);
            prStmt.setInt(1, courseId);
            rs = prStmt.executeQuery();

            while (rs.next()) {
                MoodleLog item = new MoodleLog();
                item.id = rs.getInt("id");
                item.username = rs.getString("username");
                item.eventname = rs.getString("eventname");
                item.component = rs.getString("component");
                item.action = rs.getString("action");
                item.target = rs.getString("target");
                item.timecreated = new Date(rs.getLong("timecreated") * 1000);
                item.origin = rs.getString("origin");
                item.ip = rs.getString("ip");
                logstore.add(item);
            }
        } catch (SQLException err) {
            err.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (prStmt != null) prStmt.close();
            } catch (SQLException err) {
                err.printStackTrace();
            }
        }

        return logstore;
    }

    /**
     * Возращает список курсов из БД в виде.
     * @return массив объектов MoodleCourse.
     */
    public static ArrayList<MoodleCourse> getCourses() {
        ArrayList courses = new ArrayList<MoodleCourse>();
        String query = "SELECT * FROM mdl_course;";

        Statement stmt = null;
        ResultSet rs = null;

        try {
            stmt = con.createStatement();
            rs = stmt.executeQuery(query);

            while (rs.next()) {
                MoodleCourse item = new MoodleCourse();
                item.id = rs.getInt("id");
                item.fullname = rs.getString("fullname");
                courses.add(item);
            }
        } catch (SQLException err) {
            err.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
            } catch (SQLException err) {
                err.printStackTrace();
            }
        }

        return courses;
    }

    /**
     * Возращает список пользователей из БД в виде массива объектов MoodleUser.
     * @return массив объектов MoodleUser.
     */
    public static ArrayList<MoodleUser> getUsers() {
        ArrayList users = new ArrayList<MoodleUser>();
        String query = "SELECT * FROM mdl_user;";

        Statement stmt = null;
        ResultSet rs = null;

        try {
            stmt = con.createStatement();
            rs = stmt.executeQuery(query);

            while (rs.next()) {
                MoodleUser item = new MoodleUser();
                item.id = rs.getInt("id");
                item.firstname = rs.getString("firstname");
                item.lastname = rs.getString("lastname");
                users.add(item);
            }
        } catch (SQLException err) {
            err.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
            } catch (SQLException err) {
                err.printStackTrace();
            }
        }

        return users;
    }

    /**
     * Возращает список пользователей из БД в виде массива объектов MoodleUser игнорируя пользователей
     * из списка ignoreList.
     * @param ignoreList массив пользователей, которые не должны вернутся в результате работы метода.
     * @return массив объектов MoodleUser.
     */
    public static ArrayList<MoodleUser> getUsers(ArrayList<MoodleUser> ignoreList) {
        ArrayList users = new ArrayList<MoodleUser>();

        StringBuffer usersIdBuffer = new StringBuffer();
        String usersID;

        try {
            if (ignoreList.size() == 0) {
                usersID = "";
            } else {
                Iterator it = ignoreList.iterator();
                while (it.hasNext()) {
                    MoodleUser user = (MoodleUser) it.next();
                    usersIdBuffer.append(Integer.toString(user.id));
                    if (it.hasNext()) {
                        usersIdBuffer.append(",");
                    }
                }
                usersID = usersIdBuffer.toString();
            }
        } catch (NullPointerException e) {
            usersID = "";
        }

        String query = "SELECT * FROM mdl_user" +
                ((usersID != "") ? " WHERE id NOT IN(" + usersID + ");" : ";");

        Statement stmt = null;
        ResultSet rs = null;

        try {
            stmt = con.createStatement();
            rs = stmt.executeQuery(query);

            while (rs.next()) {
                MoodleUser item = new MoodleUser();
                item.id = rs.getInt("id");
                item.firstname = rs.getString("firstname");
                item.lastname = rs.getString("lastname");
                users.add(item);
            }
        } catch (SQLException err) {
            err.printStackTrace();
        } finally {
            try {
                if (rs != null) rs.close();
                if (stmt != null) stmt.close();
            } catch (SQLException err) {
                err.printStackTrace();
            }
        }

        return users;
    }

}

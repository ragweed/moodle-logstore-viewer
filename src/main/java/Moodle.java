import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Структура данных для записи лога.
 */
class MoodleLog {
    int id;
    String username;
    String eventname;
    String component;
    String action;
    String target;
    Date timecreated;
    String origin;
    String ip;
}

/**
 * Структура данных для курса.
 */
class MoodleCourse {
    int id;
    String fullname;
}

/**
 * Структура данных для пользователя.
 */
class MoodleUser {
    int id;
    String firstname;
    String lastname;
}

/**
 * Структура данных для группы.
 */
class Group {
    String name;
    ArrayList<MoodleUser> users;
}

/**
 * Класс со статичными методами для форматирования даты.
 */
class MoodleDateFormat {
    private static SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");
    private static SimpleDateFormat formatIgnoreTime = new SimpleDateFormat("dd.MM.yyyy");
    private static SimpleDateFormat formatOnlyTime = new SimpleDateFormat("HH:mm:ss");

    static String getFormat(Date date) {
        return format.format(date);
    }

    static String getDateIgnoreTime(Date date) {
        return formatIgnoreTime.format(date);
    }

    static String getOnlyTime(Date date) {
        return formatOnlyTime.format(date);
    }
}

/**
 * Всевозможные вспомогательные методы расположены здесь.
 */
class Utils {

    /**
     * Возращает массив уникальных дат (с точностью до дня) для лога logstore.
     * @param logstore
     * @return
     */
    public static ArrayList<Date> getDateList(ArrayList<MoodleLog> logstore) {
        ArrayList<Date> dateList = new ArrayList<Date>();

        for (MoodleLog logItem : logstore) {
            Date currentDate = logItem.timecreated;

            Calendar cal = Calendar.getInstance();
            cal.setTime(currentDate);
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            cal.set(Calendar.MILLISECOND, 0);
            currentDate = cal.getTime();

            if (!dateList.contains(currentDate)) {
                dateList.add(currentDate);
            }
        }

        return dateList;
    }

    /**
     * Возращает в виде ArrayList<String> список действий, которые присутствуют в логе logstore.
     * @param logstore
     * @return
     */
    public static ArrayList<String> getActions(ArrayList<MoodleLog> logstore) {
        ArrayList<String> actions = new ArrayList<String>();
        for (MoodleLog logItem : logstore) {
            if (!actions.contains(logItem.action)) {
                actions.add(logItem.action);
            }
        }
        return actions;
    }

    /**
     * Вернёт отфильтрованный в два этапа logstore.
     * @param logstore
     * @param date - eсли date не равно null, то будут возращены записи за этот день.
     * @param action - eсли action не равно null, то данные записи будут ещё отфильтрованы и по полю action.
     * @return
     */
    public static ArrayList<MoodleLog> getFilteredLogstore(ArrayList<MoodleLog> logstore, String date, String action) {
        ArrayList<MoodleLog> filteredLog = new ArrayList<MoodleLog>();

        for (MoodleLog logItem : logstore) {
            if (date != null) {
                // Если дни дат не совпадают
                if (!MoodleDateFormat.getDateIgnoreTime(logItem.timecreated).equals(date)) {
                    continue;
                }
            }

            if (action != null) {
                // Если поля action не совпадают
                if (!logItem.action.equals(action)) {
                    continue;
                }
            }

            filteredLog.add(logItem);
        }

        return filteredLog;
    }
}
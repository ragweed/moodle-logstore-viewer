import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Хранит в статичных полях информацию из конфигурационного файла config.xml, для последующего использования
 * приложением.
 */
public class Config {

    private class XY {
        int x;
        int y;
    }

    private static ArrayList<Group> groups = new ArrayList();

    static XY mainWindowPosition;
    static XY mainWindowSize;

    /**
     * Загружает в статичные поля класса Config информацию из конфига {@param configName}.
     */
    public static void load(String configName) {
        try {
            SAXBuilder parser = new SAXBuilder();
            FileReader fr = new FileReader(configName);
            Document rDoc = parser.build(fr);
            List<Element> groupsList = rDoc.getRootElement().getChild("groups").getChildren();
            for (int i = 0; i < groupsList.size(); ++i) {
                Group group = new Group();
                ArrayList<MoodleUser> users = new ArrayList();
                group.name = groupsList.get(i).getChildText("name");

                List<Element> usersList = groupsList.get(i).getChild("users").getChildren();
                for (int j = 0; j < usersList.size(); ++j) {
                    MoodleUser user = new MoodleUser();
                    user.id = Integer.parseInt(usersList.get(j).getChildText("user_id"));
                    user.firstname = usersList.get(j).getChildText("firstname");
                    user.lastname = usersList.get(j).getChildText("lastname");
                    users.add(user);
                }

                group.users = users;
                groups.add(group);
            }
        }
        catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    /**
     * Сохраняет необходимые данные из статичных полей класса Config в конф. файл {@param configName}.
     */
    public static void save(String configName) {
        try {
            XMLOutputter outputter = new XMLOutputter();
            outputter.setFormat(Format.getPrettyFormat());
            FileWriter fw = new FileWriter(configName);


            Element root = new Element("config");

            // Описание групп
            Element groups = new Element("groups");

            ArrayList<Group> groupsInMem = getGroups();
            for (Group item : groupsInMem) {
                Element group = new Element("group");
                group.addContent(new Element("name").setText(item.name));
                Element users = new Element("users");
                for (MoodleUser us : item.users) {
                    Element user = new Element("user");
                    user.addContent(new Element("user_id").setText(Integer.toString(us.id)));
                    user.addContent(new Element("firstname").setText(us.firstname));
                    user.addContent(new Element("lastname").setText(us.lastname));
                    users.addContent(user);
                }
                group.addContent(users);
                groups.addContent(group);
            }

            root.addContent(groups);

            outputter.output(root, fw);
            fw.close();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    /**
     * Возращает список групп.
     * @return объекты Group в массиве ArrayList
     */
    public static ArrayList<Group> getGroups() {
        return groups;
    }

    /**
     * Добавляет группу {@param name} в массив groups.
     */
    public static void addGroup(String name) {
        Group group = new Group();
        group.name = name;
        group.users = new ArrayList<MoodleUser>();
        groups.add(group);
    }

    /**
     * Добавляет пользователя в группу.
     * @param groupId - порядковый номер группы в массиве groups.
     * @param userId - id пользователя, добавляемого в группу.
     * @param firstname - имя пользователя.
     * @param lastname - фамилия.
     */
    public static void addUserInGroup(int groupId, int userId, String firstname, String lastname) {
        MoodleUser user = new MoodleUser();
        user.id = userId;
        user.firstname = firstname;
        user.lastname = lastname;
        groups.get(groupId).users.add(user);
    }

    /**
     * Переименовывает группу.
     * @param groupId - порядковый номер группы в массиве groups.
     * @param groupName - новое имя группы.
     */
    public static void renameGroup(int groupId, String groupName) {
        groups.get(groupId).name = groupName;
    }

    /**
     * Удаляет группу {@param groupId} из конфига.
     */
    public static void delGroup(int groupId) {
        groups.remove(groupId);
    }

    /**
     * Возращает список пользователей из группы {@param groupId}.
     * @return массив объектов MoodleUser.
     */
    public static ArrayList<MoodleUser> getUsersInGroup(int groupId) {
        return groups.get(groupId).users;
    }

    /**
     * Удаляет пользователя {@param userId} из группы {@param groupId}.
     */
    public static void delUserFromGroup(int groupId, int userId) {
        groups.get(groupId).users.remove(userId);
    }

}

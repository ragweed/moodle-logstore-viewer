import java.awt.Dimension;
import java.util.ArrayList;

/**
 * Приложение для просмотра журналов активности пользователей на курсах системы ДО Moodle.
 */
public class MoodleLogstoreViewer {

    /* Переменная организована таким образом, чтобы иметь возможность обновить список групп после закрытия окна
     редактирования групп.
      */
    static MainWindow mainWindow;

    public static void reloadGroups() {
        mainWindow.pasteGroups(Config.getGroups());
    }

    /**
     * Отображает окно соединения с БД Moodle.
     */
    public static void openConnectWindow() {
        ConnectWindow connectWindow = new ConnectWindow();
        connectWindow.pack();
        connectWindow.setTitle("Соединение с базой данных");
        connectWindow.setLocationRelativeTo(null);
        connectWindow.setVisible(true);
    }

    /**
     * Отображает окно программы.
     */
    public static void openMainWindow() {
        ArrayList<MoodleCourse> courses = Database.getCourses();
        ArrayList<MoodleUser> users = Database.getUsers();
        ArrayList<Group> groups = Config.getGroups();

        mainWindow = new MainWindow();
        mainWindow.pasteCourses(courses);
        mainWindow.pasteUsers(users);
        mainWindow.pasteGroups(groups);

        mainWindow.pack();
        mainWindow.setTitle("Moodle Logstore Viewer");
        mainWindow.setSize(new Dimension(700, 500));
        mainWindow.setLocationRelativeTo(null);
        mainWindow.setVisible(true);
    }

    /**
     * Отображает окно редактирования списка групп.
     */
    public static void openEditGroupsWindow() {
        ArrayList<Group> groups = Config.getGroups();
        EditGroupsWindow editGroupsWindow = new EditGroupsWindow(groups);
        editGroupsWindow.pack();
        editGroupsWindow.setSize(new Dimension(700, 500));
        editGroupsWindow.setLocationRelativeTo(null);
        editGroupsWindow.setVisible(true);
    }

    /**
     * Выполняет необходимые процедуры и завершает работу программы.
     */
    public static void exit() {
        Config.save("config.xml");
        Database.close();
        System.exit(0);
    }

    /**
     * Поехали!
     * @param args
     */
    public static void main(String args[]) {
        Config.load("config.xml");
        openConnectWindow();
    }

}
